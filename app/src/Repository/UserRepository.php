<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param Request $request
     * @return array|int|string
     */
    public function findAllUsers(Request $request)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u.id, u.email, u.firstName, u.lastName, u.age');

        if ($request->query->has('offset') && $request->query->has('limit')) {
            $offset = $request->query->getInt('offset');
            $limit = $request->query->getInt('limit', 20);
            $qb->setFirstResult($offset)->setMaxResults($limit);
        }

        return $qb->getQuery()->getArrayResult();
    }
}
