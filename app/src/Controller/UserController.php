<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 */
class UserController extends AbstractController
{
    use Traits\UserTrait;

    private UserRepository $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @Route("/user", name="user_all", methods={"GET"})
     * @var Request $request
     *
     * @return JsonResponse
     */
    public function getAll(Request $request): JsonResponse
    {
        $result = $this->userRepository->findAllUsers($request);

        return $this->json($result);
    }

    /**
     * @Route("/user/{id}", name="user_one", methods={"GET"}, requirements={"id": "\d+"})
     * @var User $user
     *
     * @return JsonResponse
     */
    public function getOne(User $user): JsonResponse
    {
        $result = $this->viewUser($user);

        return $this->json($result);
    }

    /**
     * @Route("/user", name="user_create", methods={"POST"})
     * @var Request $request
     *
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $result = $this->saveUser($request);

        $response = ($result === true) ? ['status' => 'success'] : ['status' => 'failed', 'errors' => $result];

        return  $this->json($response, Response::HTTP_CREATED);
    }

    /**
     * @Route("/user/{id}", name="user_update", methods={"PUT", "GET"}, requirements={"id": "\d+"})
     * @var Request $request
     * @var User $user
     *
     * @return JsonResponse
     */
    public function update(Request $request, User $user): JsonResponse
    {
        $result = $this->saveUser($request, $user);

        $response = ($result === true) ? ['status' => 'success'] : ['status' => 'failed', 'errors' => $result];

        return  $this->json($response);
    }

    /**
     * @Route("/user/{id}", name="user_delete", methods={"DELETE", "GET"}, requirements={"id": "\d+"})
     * @var User $user
     *
     * @return JsonResponse
     */
    public function delete(User $user): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->json(['status' => 'success']);
    }
}
