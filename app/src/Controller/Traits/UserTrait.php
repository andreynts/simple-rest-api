<?php

namespace App\Controller\Traits;

use App\Entity\User;
use Symfony\Bundle\MakerBundle\Validator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints as Assert;

trait UserTrait
{
    /**
     * @param User $user
     * @return array
     */
    private function viewUser(User $user): array
    {
        $payload = [
            'id' => $user->getId(),
            'email' => $user->getEmail(),
            'first_name' => $user->getFirstName(),
            'last_name' => $user->getLastName(),
            'age' => $user->getAge(),
        ];

        return $payload;
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return array|bool
     */
    public function saveUser(Request $request, User $user = null)
    {
        if ($user === null) {
            $user = new User();
        }

        $data = json_decode($request->getContent(), true);

        $errors = $this->validateUser($data);
        if (!empty($errors)) {
            return $errors;
        }
        $user->setEmail($data['email']);
        $user->setFirstName($data['first_name']);
        $user->setLastName($data['last_name']);
        $user->setAge($data['age']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return true;
    }

    /**
     * @param array $input
     * @return array
     */
    private function validateUser(array $input): array
    {
        $constraint = new Assert\Collection([
            'email' => [
                new Assert\NotBlank(),
                new Assert\Email()
            ],
            'first_name' => [
                new Assert\NotBlank(),
                new Assert\Length(['min' => 2, 'max' => 100])
            ],
            'last_name' => [new Assert\Type('string')],
            'age' => [
                new Assert\Positive(),
            ],
        ]);

        $validator = Validation::createValidator();
        $violations = $validator->validate($input, $constraint);

        $errors = [];
        if (count($violations)) {
            foreach ($violations as $violation) {
                $errors[] = [$violation->getPropertyPath() => $violation->getMessage()];
            }
        }

        return $errors;
    }
}
