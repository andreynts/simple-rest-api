#Simple restFul CRUD API with Docker + PHP + Nginx + MySQL + Composer
Docker Compose configuration to run PHP 7.4 with Nginx, PHP-FPM, 
MySQL 5.7 and Composer.

## Overview

This Docker Compose configuration lets you run easily PHP 7.4 with Nginx, 
PHP-FPM, MySQL 5.7 and Composer.
It exposes 4 services:

* nginx (Nginx)
* php (PHP 7.4 with PHP-FPM)
* db (MySQL 5.7)
* composer

The PHP image comes with the most commonly used extensions and is configured with xdebug.
Nginx default configuration is set up for Symfony 5 (but can be easily changed) and will serve your working directory.
Composer is run at boot time and will automatically install the vendors.

## Install prerequisites

For now the project has been tested on MacOS only but should run fine on Docker for Windows and Docker for Linux.

You will need:

* [Docker CE](https://docs.docker.com/engine/installation/)
* [Docker Compose](https://docs.docker.com/compose/install)
* Git (optional)

## How to use it

### Starting Docker Compose

Checkout the repository or download the sources.

Simply run `docker-compose up` and you are done.

Nginx will be available on `localhost:80` and MySQL on `localhost:3306`.


### Using Composer

`docker-compose run composer <cmd>`

Where `cmd` is any of the available composer command.

### Using MySQL

Default connection:

`docker-compose exec db mysql -u root -p`

Using .env file default parameters:

`docker-compose exec db mysql -u root -p docker_db`

If you want to connect to the DB from another container (from the `php` one for instance), the host will be the service name: `db`.

### Using PHP

You can execute any command on the `php` container as you would do on any docker-compose container:

`docker-compose exec php php -v`

## Change configuration

### Configuring PHP

To change PHP's configuration edit `.docker/conf/php/php.ini`.
Same goes for `.docker/conf/php/xdebug.ini`.

You can add any .ini file in this directory, don't forget to map them by adding a new line in the php's `volume` section of the `docker-compose.yml` file.

### Configuring MySQL

If you want to change the db name, db user and db password simply edit the `.env` file at the project's root.

## How to use API

#### Important! Before using api, you need to apply migration
Run the command:

`docker-compose run php php bin/console doctrine:migrations:migrate`

### Show all users
`GET http://127.0.0.1/user`

There is pagination support, you need to pass additional parameters:
`http://127.0.0.1/user?offset=0&limit=1`

### Show one user
`GET http://127.0.0.1/user/1`

### Create new user
`POST http://127.0.0.1/user`

raw object
```
{
    "email": "orange@gmail.cc",
    "first_name": "orange",
    "last_name": "fr",
    "age": 25
}
```

### Edit user
`PUT http://127.0.0.1/user/1`

raw object
```
{
    "email": "pomegranate@gmail.cc",
    "first_name": "pomegranate",
    "last_name": "apple",
    "age": 25
}
```

### Delete user
`DELETE http://127.0.0.1/user/1`
